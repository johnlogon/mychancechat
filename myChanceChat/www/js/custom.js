/*
	Author: Suhail
*/

//override console writing; show log only for debug=true

var consoleHolder = console;
function debug(bool){
    if(!bool){
        consoleHolder = console;
        console = {};
        console.log = function(){};
    }else
        console = consoleHolder;
}
debug(false);



var busy = false;
var limit = 15;
var offset = 0;
var again = true;

jQuery.expr[':'].Contains = function(a,i,m){
    return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
};

window.setInterval(function() {
    if (!app.checkConnection()) {
        //console.log('showconnectionerror');
        showConnectionError();
    } else if ($("#network-error").css('display') == 'block') {
        hideConnectionError();
    }
	if(!$('#loginPage').is(':visible'))
		appStartup();
}, 5000);

$(document).ready(function(e) {
   	window.setInterval(function() {
		updateLastActivity();
	}, 5000); 
});

function showConnectionError() {
    $("#loginPage").css('display', 'none');
    $("#homePage").css('display', 'none');
    $('body').css('background', 'none');
    $("#splash").css('display', 'none');
    $("#network-error").fadeIn(1000);
}

function hideConnectionError() {
    $("#loginPage").css('display', 'none');
    $("#network-error").css('display', 'none');
    $("#homePage").css('display', 'block');
    $('body').css('background', 'url(../img/background.jpg)');
}

function getUrl() {
    var ossn_site_url = "http://mychance.in";
    return ossn_site_url;
}

function getOssnDatafolder() {
    var ossn_data = "/ossn_data/"
}

function openSpinner() {
    //navigator.splashscreen.show();
    //$("#overlay").show();
    $("#loginPage").css('display', 'none');
    $("#homePage").css('display', 'none');
    //$('body').css('background', 'none');
    $("#network-error").css('display', 'none');
    $("#splash").fadeIn(500);

}

function closeSpinner() {
    window.setTimeout(function() {
        //$('body').css('background', 'url(../img/background.jpg)');
        $("#network-error").css('display', 'none');
        $("#splash").fadeOut(500);
    }, 3000);
    // $("#overlay").hide();
}

function storage() {
    return window.localStorage;
}
// First route to show
var GLOBALSTATE = {
    route: '.list-text'
};

// Set first Route
setRoute(GLOBALSTATE.route);
$('.nav > li[data-route="' + GLOBALSTATE.route + '"]').addClass('active');

//dirtiest, ugliest, hackiest ripple effect solution ever... but they work xD
$('.floater').on('click', function(event) {
    /*var $ripple = $('<div class="ripple tiny bright"></div>');
    var x = event.offsetX;
    var y = event.offsetY;
    var $me = $(this);

    $ripple.css({
        top: y,
        left: x
    });
    $(this).append($ripple);

    setTimeout(function() {
        $me.find('.ripple').remove();
    }, 530)
*/
});

function setMessageToHidden(id) {
    $('#message-to-hidden').val(id);
}

function setUsernameHidden(username) {
    $('#username-hidden').val(username);
}

function setAgain(){
	busy = false;
	offset = 0;
	again = true;
	
}
// Have to Delegate ripple due to dom manipulation (add)
$('ul.mat-ripple').on('click', 'li', function(event) {
	$('.online-span').hide();
    var friend_id = $(this).attr('id');
	again = true;
    setUsernameHidden($(this).attr('data-username'));
    if (friend_id != 'account_tab' && friend_id != 'text_tab' && friend_id.substring(0, 4) != 'chat') {
		$('#bottom-ul').hide();
		document.addEventListener("backbutton", onBackKeyDown, false);
        var userid = friend_id.substring(6, friend_id.length);
		$("#friend_dp").attr("src","");
        loadMessages(userid);
        setMessageToHidden(userid);
        $('#select-media').show();
		$('#friend_dp').show();
    } else if (friend_id.substring(0, 4) == 'chat'){
        $('#select-media').show();
		$('#friend_dp').show();
	}
    else{
        $('#select-media').hide();
		$('#friend_dp').hide();
	}
	
    var friendName = ($(this).find('span').html());
    if (friendName != undefined)
       { $('#friendName_header').text(friendName).addClass('special_class');
	   if($("#username-hidden").val().length>0)
	   $("#friend_dp").attr("src",getUrl() + "/avatar/"+$("#username-hidden").val()+"/small");
	   
	   }
    else
     {   $('#friendName_header').text("").removeClass('special_class');
	 	 $("#friend_dp").attr("src","");
	 }
});

function onBackKeyDown(e) {
   e.preventDefault();
   document.removeEventListener("backbutton", onBackKeyDown);
   loadAgain = true;
   $('#bottom-ul').show();
   $('#text_tab').click();
}

// Set Name
// setName(localStorage.getItem('username'));

// Dyncolor ftw
if (localStorage.getItem('color') !== null) {
    var colorarray = JSON.parse(localStorage.getItem('color'));
    stylechange(colorarray);
} else {
    var colorarray = [15, 157, 88]; // 15 157 88 = #0f9d58
    localStorage.setItem('color', JSON.stringify(colorarray));
    stylechange(colorarray);
}

// Helpers
function setName(name) {
    $.trim(name) === '' || $.trim(name) === null ? name = 'John Doe' : name = name;
    $('.card.menu > .header > h1').text(name);
    //localStorage.setItem('username', name);
    //$('#username').val(name).addClass('used');
    $('.card.menu > .header > h3').text(name);
}

function setDp(dp) {
    $.trim(dp) === '' || $.trim(dp) === null ? dp = 'img/index.png' : dp = dp;
    $('.card.menu > .header > img').attr('src', dp);
}

// Stylechanger
function stylechange(arr) {
    var x = 'rgba(' + arr[0] + ',' + arr[1] + ',' + arr[2] + ',1)';
    $('#dynamic-styles').text('.dialog h3 {color: ' + x + '} .i-group input:focus ~ label,.i-group input.used ~ label {color: ' + x + ';} .bar:before,.bar:after {background:' + x + '} .i-group label {color: ' + x + ';} ul.nav > li.active {color:' + x + '} .style-tx {color: ' + x + ';}.style-bg {background:' + x + ';color: white;}@keyframes navgrow {100% {width: 100%;background-color: ' + x + ';}} ul.list li.context {background-color: ' + x + '}');
}

function closeModal() {
    $('#new-user').val('');
    //$('.overlay').removeClass('add');
    $('.floater').removeClass('active');
    $('#contact-modal').fadeOut();

    $('#contact-modal').off('click', '.btn.save');

}

function setModal(mode, $ctx) {
    var $mod = $('#contact-modal');
    switch (mode) {
        case 'add':
            $mod.find('h3').text('Add Contact');
            break;

        case 'edit':
            $mod.find('h3').text('Edit Contact');
            $mod.find('#new-user').val($ctx.text()).addClass('used');
            break;
    }

    $mod.fadeIn();
    //$('.overlay').addClass('add');
    $mod.find('#new-user').focus();
}

$('.mdi-arrow-left').on('click', function() {
    $('.shown').removeClass('shown');
    setRoute('.list-text');
});

// Set Routes - set floater
function setRoute(route) {
    GLOBALSTATE.route = route;
    $(route).addClass('shown');

    if (route !== '.list-account') {
        $('#add-contact-floater').addClass('hidden');
    } else {
        $('#add-contact-floater').removeClass('hidden');
    }

    if (route !== '.list-text') {
        $('#chat-floater').addClass('hidden');
    } else {
        $('#chat-floater').removeClass('hidden');
    }

    if (route === '.list-chat') {
        $('.fa-bars').hide();
        $('.mdi-arrow-left').show();
        $('#content').addClass('chat');
    } else {
        $('#content').removeClass('chat');
        $('.fa-bars').show();
        $('.mdi-arrow-left').hide();
    }
}

// Colorpicker
var cv = document.getElementById('colorpick');
var ctx = cv.getContext('2d');
var img = new Image();
// Meh .. Thx 2 Browser Security i need BASE64
img.src = 'data:image/gif;base64,R0lGODlh8gDYAPepAP///8yZ/zNm////Zsz/ZmbM//+ZZplm/zPMzMz/M//MZmb/mZkz/8wAzMz/mcwA/wD/mf//AMwz//8A//8AAP/MmTPM////mZn/mf8AZmb/zGb/ZgD/zAD//5mZ/wCZmf+ZzAD/AP+Z/wDMAJn/Zpn/zDPMM5nM//9mzP+Zmcxm/5nMAP8zmf9QUGaZ/2YA/wBmzGb//zMz/wBmmQAA/wAzzDMzzDOZ/wBm/2aZAACZAADMZgCZzP8zzJn/M8wzmWZm/wDM//8zAMwAAGb/M8wAmQCZM8zMAMxmmQCZ//9mAMxmAADMmf9mmcyZAP/MAMwAZv9mZv9m//+ZM/+ZAMz/zMz//8zM/zOZMwBmZpmZZmZmM5kAmZkAzGYAM2YAzJkAM5kA/2ZmmQBmADNmAJkzmTMzAGaZmTMzmZlmMzOZZpkzZpkAADNmzMwzAJkzADNmmZlmAIAAAAAzmQAAzAAAmf//zAAAZmYzAAAzZpkzMwAzAGYAZv/M///MzAo7CmwKbLFjYwo7bGw7CgoKbJ1sCmOKsQo7nZ0KCs47CoUKCgoKnQoKzmOK2J07CqmpxgpsbGwKzmNjsbFjsZ0K/4qKYzs7CjtsCrGKY7FjigpsCmOxY50KO2wKO2OxisbGqZ0KnanGxp0Kzv/Ozv//zs7O///O/87//87/zv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxNUIxREUxODJBRTMxMUU1ODYyMkREQ0Q3NUZENjdFMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxNUIxREUxOTJBRTMxMUU1ODYyMkREQ0Q3NUZENjdFMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjE1QjFERTE2MkFFMzExRTU4NjIyRERDRDc1RkQ2N0UwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjE1QjFERTE3MkFFMzExRTU4NjIyRERDRDc1RkQ2N0UwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAqQAsAAAAAPIA2AAACP8AUwkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSoUUFIHxpa+rCR04eHoj5cRPUho6sPCWk1uhOpoDxeFy41BGfsQqeN2qBdGPXQnLYLqS6qI3fhVUZ07i7USugOX642veYZPDiswbFwEic2axBtm8eP1xpsO6dyZbgG5dbZvLmuwbt0QofWa5DvndOn/wKGKZiwa7BJBSJWTLssU4GOIetW+1QgZcvA30oVqJmzcbpVBYIWzTwvVoGmUUv3u3X1ytavsyOdXbv70ty7wzv//R28fNTix9NTXd68/dXo0+NXt47ya/b7hLvrrx2+/+7yAAaX3oDHtWdgc/ElKB0h9KVkH3737SchHP5V2EaAGM5B4IZ1HOghHQqGyGCDJz0I4WsT7mehfxkGyCGBHx4YooIjkliSiSfml2J3K/bXIoAvDhijgTMmWKONI+GYYx478tjjfz8KGGSBQyJY5HRHIhmSkjk2yd+TukUp5ZScVWnllahlqeVHXJ7oJW1ghimmZWQaZyZzaC64ZpJLovhmYnFCNieddW52p2h5prmnSG1C+CeggV44qIaFdngoiIn6teiWfbr2KIWRTkppoZdimqiam2YECSRZdJrHDLC+/8nDrHHCYOucNeRaJw283inDr4lKImyqGq3KahbI5gjrsrFOOOuztFpo67S3ZpjrtbpyyOu2vX7467fAzojGuGgIKwmxFRmL7Lrr3sfsu83WBu280e5G7b3VBoftvtkex+2/3TYH7sDhTkfuweUOi+5D6rLrcLKDwStxvPRWXC++GOfL78b9AuxxwASHXDDCJCd87sIKNfzwyhO3PIPFMPOQ8cwwcGxzDR/nTIPIPMtQ8s8Jo5zQsSsX7fLEMVtMc8Y3c6zzxz2LDPTPJwttENFFP3y0xElXvDTGTW/8tMdRhzx1yVVbTRDWWbO7Nbxd0/s1vmHzOzbAZRN8Nslpq/8tENttI/v2u3HPO/e9de979795D7w3wn37DXjggzNbOLSHU5s4totz2zi4jx8cudqTt135spc/m/m0m1/b+baffxs6uaNbXXrWp8Oa+qyr29p6rq/zGvuvs49bu9C3G5377jL3/jvOwQ/vc/HHo5w8y8vv3nvNvwe/8/DFl+t3QddrnX3q2z/vvfThV49uKKGcETi7H9RfOQL4Xx7E/pkn4f/mOAhg5wRAwM/Z4ICze4QC/Qa/+J3hgYGrnwTt5zL8WTB/MdufBvlHM/958H83C6AIBagzApqwgD07oAoRODUxuFAMCnwEyhr4wBrWcGUTzCEF33XBHmJwXhsMIgf/7/XBIoJwXyNMIgn/dcImonBgK4wiCxH2wirCcIGboqENtwhBZOnwizv0oRh/KMQyDtGIaDyiEte4RCe68YlSjOMUrUjHK8pQS1rkoh7ByMcPjPGPCDCjIIOQxkImgY2IxMEbFykAOTrSBnWM5BXxqMdKPrCPYATkGAdpRkOmMZFsZOQbHylHSUbyjjZyoCW5iMkvalKMnCyjJ9EIyjWK0o2kjKMp64hKEqlylTZspQ5f6cNYCnGWRqylEm/pxFxKcZd07GWDfgnMSwpzgsTsoTGDiMwiKjOJzGyiM6MITStKkz7UrOY1sZlNC25zg9384DdHGM4TjnOF5aziOa2T/05grlOC7XTnO/cXTw/OU4T1NOE9VZjPF+5zNf1c5T/rF1D8DZSgBT3kQRWZ0EYuFJINhSElqxnMiVY0kBfNqEYP2lGPLjSkIkVSRC05UT9W9KKEzOhGOZrQj4K0oQ8FzEwrWdOT4lSlO22pT2EaVKN4whNqIOkDmUDVdXLgqu3sgFbfaYGuxvMGYJ2nC8ZaTyCY9Z4vSGtDI8FW+jwVqmqIKzCpSteq9vGqeMUqILXK160OsquA9aohwUrYsCZyrIglKyPNytizPjKtkFWrKb9A2S+wNRJceWtcN7tZPdb1s3bNYV5Hq9ce9vW0fg1iYFcr2CIW9rWGTWJiZ6vYJv829raOjWJkdytZK1b2t5Ztq1A0y9niynWqoE0uEyRI2uaWFrXQTS1rp9ta2Fo3trTNbm1xy93c8va7vQWueIOLWZ8Q17joVa56meDc9nIguvDtAHXna4Hr2vcG2s2vC7rLXyCA978vGK+Ag2te9Bo4rutVrnudG9/o0pe6972ufrXb3+4CGLwDFnB5eQLXAxs3wcldcHMbDN0HTzfC1p1wdivM3Qt/N8Pj3fBOOuxhzoIYtCImLYlRa2LWohi2KqYti3HrYt7CWLwy1gmNa4zgG9c1x6Pd8Wl7vNofvzbIsx3ybYu82yMDN8k5WTKTnfxkKONVyn2lcmCtXFgsJ1b/y43lcmS9/Fsw40TMNSYzXc18ZjRrVc2AZTNh3YxYODNWzpClc2XtfBM8e1jPVOXzVf38Z0DXV9D4JfR+De1fRAdY0ZYtMJNtDGlJv5fSlr60oDW9aUN7+tOKZrRNHH1gSLNX0pSWr6UxnWlCc7rTiAZ1qHtCawPb2tS5TjWvWf3rVwtb1jTZxCawMOq47uDaZIaAts2sgW6jOQbgVnMBxs3mE5jbzR5IN5wPwG45M+DdiqaEvHMi7Wlj4d41vra+sb1ebft72+7ttsC9HV9wGzzc9B23wsl9X3M7/Nz6TbfE1d1fdlu83QB+t8bhneEweDwM8qZETep975KXHL37/045vz/775YDfLQDjznBT3vwmiN8tQvPOcNf+/CeQ3y2Ew86xW978aJjfLcbTzrHgfvxpoN83jAhucmnjm9rq/zqO6Cry7f+cpl7feY2D/vNdU72nfv87D8XutqHbvS2H13pcF+60+f+dJGzROpUzzvW974DrvsdAl8PvAbETvgYlP3wBUC74k+w9sZ7wO2QP0DcJ88Aulv+6XfPu+bvzXes/53rgv964cWO+LIvHu2OX3vk3U75uF/e8nZXib03T/XOX/3zWw+910cf9tKT/fRnT73aV9/21sP99XSPfUpmT3uT217luHe57mXOe5v7XufA97nwhU58oxtf6cifu//yUcL85nP++fuOfsunH/Pq1/z6Oc9+z7cf9O4X/ftJD7/Tx3+S8psf/emnfv7GfgPnfgcHfwsnfw9HfxNnfxeHfxunf03Hfybhf80HgPomgANIgN1mgAaHgAqngA7HgBLngBYHgRongR9HgSVhgbSHgdemgdrGgR3ogYYHgokngoxHgo9ngpKHgpWngiCXeebnfDAog4BHgzZ4gyCogztIgj74gygohEO4Ei64eTDYdzJIg4NngziYgyLIgz1ogkAYhCrIgiRxhZqXhUjIhUv4hU4ohlFYhlSIhiKhCZowBkV4byPQhwAYAoAogAswiARYAoZogFaQiAh4BYyogAH/8IgMqAKS6IASUIkQ+ACYqIKisIkngYd5OAag2Hx9OIp+yHeAeIqB+HeDuIqEKHiG+IqHWHiJOIuKiHiMeIuNuHiPuIuQ6HiS+IuTGHmVOIyWSHmYeIyZ+HpdsIxdsImiMBKeCIrSKI15R4rWWIoph4ramIotx4re2IoxB4viGIs1R4vmWIs5h4vqmIs9x4vu2ItBB4zyGIxFR4z2WIxJh4z6mIxOx4z+2Iyc6BHROI0EGYp8eI0IOQL6to0MyY3f+JDgOI4SSY7nWJHouI4YyY7vuJHwOI8eSY/3GJL4uI8kyY//eJIA+YwaMZAF2ZIJ+ZIj0JAyGQIQWZMLMJE4/1kCFrmTVpCRPnkFHBmUAfCRRKkCInmUElCSSvkAKNmUALmSLRmVoAiTCTmTDWmTEJmTE8mTFvmTGSmUHFmUH4mUIrmUJemUTamSGPGJUlmQVImQVsmQWPmQWimRXFmRXomRYLmRYumRZBmSZkmSaImSankRbNmW0/iW1xiX2ziX31iX43iX55iX67iX79iX8/iX9xiY+ziYJ1mYFnGYiDmVikmKjKmNjumNkCmOkmmOlKmOlumOmCmPmmmPnKmPnvmPoFkRojmapWmap3mKqcmKqwmLrUmLr4mLscmLswmMtUmMt4mMuemPu0kRvYmYvzmKwSmcwzmIxfmKxzmLyf95i8u5i835i885jNF5jNPJjNU5EdfZltnZh9sJiN3pnd+pk+HZk+MJlOU5lOdplOmZlOvJlO3ZjFA5mok5n/VJk/eZn/oZnv3pn+UZoAKangVqoO35nhIRn1I5nzFZn/d5k/m5n/w5nv8JoOc5oAS6ngeKoBnhoVEJog06ohAanqdwCqWwoztKmSlqoSyaoS/KoRDxB3+wBwoKikawpL9pAk4anBsQpcOJAVRanFVwpXeZo6cAAFzKpTz6k30QpsspAmTanFJwps85AWoanQ3Qpu0JCHBaEUZ6pHtQp4i5pHjKpDDppHz6pDMZpYAqpTZJpYRapTl5pYiKpRWppV3/2qhe2qPqGKaSKqZCSaaWWqZFeaaaiqZIqaaeuqZL2aai6qZoyQemygdwCggRMad12qqt2pJ5Gqt6ao19Wqt+qo2BmquC6o2F2quGKo6JGqyKmoiM6qjGCgBfegWTuqyU6o6X+qyYKo+bOq2cao+feq2gqo+juq2k+o+n+q2oGqcMwaquWq52qqSymq5GMIq22q63qqvwuqu+Oq+/Kqz2iqU5eqz66qWmwKz+2ge7CK0CG63UWrDViq0Im63curDdCq4OG66qihDkaq4Uq64WawTumrEmEK8cuwH0+rEYcK8iiwr7WrIAYAr9+q/LOrAsKwIG+7JSkLAyOwEMW7MN//CwOBuuEkuxPFunF6uuGuuuHRuvIEuvInuvJGuy+5qyKiupLTuwMGuwM5uwNsuwOYuzEWsQdNqz5vqz6Rq07Tq08Fq083q09pq0SnusTNu0ffC0Ahu1BTu1CFu1C3u1D5u1BbG1XOuqXiurYGurYqurZOurZiusaJu2jrq2Teu20Aq31Cq32Eq33Gq3Dou3BKG3e+uzfZunf1urgZurg9urhRush4u4Xaq4Ksu4z+q40wq51yq520q54Gq5A4G5mbu5nNu5fPq5gRq6hTq6iVq6pnuybOu0qmuprLuprvupsDuqsvuttCsQtru3uIunuru7vBulvkuowIuowmu6qP/7r8eLvMl7psvrqc0rqs97qtGbCtPLtdW7pNfrpNmrvdsbst1bBd+LuOHrr+NLpuVrvudLs+l7s+uLqjubuXwbv/O7sfV7v/jbvfubtv3LrP/rsgE8wAScvgeMwAfxvj0bvxg7v/Xrsfebv/o7vIlbvGF6wQEcswNcwAa8vu07sbfLwCT8wCecvxOstBW8sv/7whoswwcsrhJrpJlLBkqMuzrQxLpLBFDMuyQwxb7rAFbcvaSgwo36w5IKAl58vCgQxsnbA2S8vEVwxs1bBmpMuanaEDbsqkocx0t8sU1cx06ssVCcx1HcsVPcx1QMslYcyFd8tHZgB1msxVw6Cmz/68WM/MUtG8aQLMYwS8aUXMYze8aYjMY2q8acvMZ3a8QOYcNyPMpzHKt2fMp3XKt6vMp7nKt+/Mp/3KuCPMuDHKyFXMikkMsqPAq87Ad+4K+NHMyO/KyRXMySPK2VnMyWfK2Z3MyavK2dHM2ebKptTBFITMrYXMqovM2pzMre3MqwHM6xTMvkXMu3fM6GrMtKy8uj4Mvu/MthKszyPMzGXM/HrMz4vMzOvM/PLM3+rMbVbBGXkM0Ezc0GrQPfnNBEIM4MTQLl/NAOgM4Snc6HfKzs/M4YPc8aDQL23NEokM8g3QP8PNJF8M8mPQkaMdAEjc0Hzc0K/c0NLc4QXc4T/z3RuVzRidzLGJ3RGy3PHm3PIZ3PJM3PJv3PKJ0RKr3So9zS2/zS3hzT4TzT5FzTNZ3L7KzTO83TPR3MP13PQY3PQ73PRe3PR40RSa3UcczUqOzUrAzVsCzVtEzVVJ3VdO3OWy3MXW3MX63MYe3MYy3NZX0RZ43WZKDWp8zWq+zWrwzXsyzXNV3XdX3XXJ3XkbzXydzXzfzX0RzYAk3YcmzYdozYeqzYfszYguzYEw3ZdC3ZjUzZlW3ZlIzZmazZnczZFTHYaA3adSzaeUzafWzagYzaEq3aWc3ajOzakAzbsS3bZ0zbnGzbFIHbSq3bTczbUOzbUwzcVizc6EzcO/9t3F6M3GGs3GTM3M3t3GUA3RMh3StN3Qht3djt0NrN3efs3VrN2uL90eRt3iWN3uotEexd0NRt3QuN3dod0fRdyPb9zuDN0eJN3iJt3uid3il9CQFOyu5N4PF94Amu4Avuyw2e3xDO3/793+tt4WhtBioO2jnQ4qLtAzBO2gQw46Z9ATZO3xWQ4wueAjzO2k3w467NAkIO2z9Q5LLNBUj+15Ow5B9h4eyt4lC+4gfd4lTu4goN41ge4w0941xO4xBt42B+43Kd42Su45DN42je4xv942wO5B4t5HA+5CFd5HRu5CSN5Hie5GTN5CLh5HIc5YAu5adc5YRu5auc5Yj/ruWv3OWM7uWzHOaQLuboXOaUbuYYneaYrubB3Oac7ubFHOegLufJXOekbufNnOeorudqvOQm3hEoHuiwLuiFPuuGnui2ruiNnuuOHum8LumV/uuWnunCrumdXuyeHurILuqlvuymnurOjuSsrhKWEOvUTuvWngO3nu0+oOvcTgC9/u0XAOziXgHDXu4pYOzo3gTJvu4swOzu/gPPHu+gwBLTTu2wfu20ru233u26Du69Pu7Abu7Dnu7Gzu7J/u7MHu/PPu8rUe/2Duj4Puv6buv8nuv+zusA/+sCL+wEX+wGj+wIv+wK7+wML+0PH+gRX+gTn+gV3+gXH+kZX+kb/5/pHd/pHx/qIV/qI5/qJZ8SDn/yKp7yhL7yiN7yjP7ykB7zlD7zmF7znH7zoJ7zpL7zqN7zKPHzQC/0VU70WW70XY70Ya70Zc70ae70bQ71cS71dU71eW71J4H1J6/1VM71WO71XA72YC72ZE72aG72bI72cK72dM72eO72JgH3Dy/3LU73MG73M473Nq73Oc73PO73Pw74Qi74RU74SG74JYH49q742M74ju/tkC/55E75lq/umK/58M75nk8SoF/tis/42+74kB/ukk/55275mN/ums/5XBD7I2EJxg/0QV/7pI/7p7/7qu/7rR/8hA8KxC/7x//wW5D9Wr8C3P/P9Qnw/V4/AOIP9gpQ/mJvAOhP9lGw/mafAe6P9kgQ/2q/BvS/89Rf/Yd//YGe/fwPEFu25CBY0GCOFQkVrvDR0OFDHwkkTkxAwOJFjAQGbOQ44MJHkCEvKCBZUkEFlClVVjDQ0qWBFDFlzkwRxebNKE107uTZJMNPoBlYDCValAUSpEmR/GDa1OmPNVGlruFS1WpVUFlTbeXa1etXsGHFhrVU1sxZMwLVrhV4kOBCuAkhNqRYV2JGix31bhT50eRfkitRviTckmZMnIlt9tQZ1PFPo0OVTkb6lOlUzFGvZgU11vNn0J7NsiW9tmBc1HLp2mVdMe9e2B79AqZ9cnD/YdwwESvmnbPxY+BCJVMmvvRyZuRrOIdm3px5pdLRBaamvqL19QSxtQ+o3V1BbvAGeo+PEtx8huLpkSRnn8n5e/hhoUsvXT019tbbY3uvHT43+d7OC0694thLzr34EoxvPvrYsg81/FjTDzb+aPMPNwB5ExA4AokzEDkEFRTxuQZJezCuCO2acK8KAbuwsAwV2/CxDin7MLMQR9RxLAZLnO7EhVKsa0W9WvzrRcJiTGxGx2qc7EbMctxxSq969BHIIIWciMiOjDQJyZeUxInJoJxUCsqppKRyTStLxFIhLbfkciMvSwLTJTFvIhMoM5NCUyo115yyzQbfTChOieak/7PO7+4UL8/y9kSvz/X+XCNQQXUklD5DrUNUUe4YdfTRPCWdtE9LL800002l6xTR7BRltNE7IY10T0or/RPTVRWs5Fcff3wTVlBnHdVWU3O1NBNee/UV2Aa1kBbLI6rVMgJsuXxiWy+n8BZMJcIVswVyyYTiXDO9UPdGZpt1VsRfN5V23mmrq/Zea7HDdt9st9v2X26983bgb8ML92BxySN34XLPO/dhdNVTd+J1QWT2XYzjXYtejuuFC1+Q862LX5L71QtglAP+i2CWCyYMYZgTToxhmht2DGKcI56MYp4rjqpdjIPeCtiOi/Y4ZKRFLnlpk1N2WuWWo3Y5Zqplrv/5aptz1lrnnrtWF2ihw/7EaLKTNvsIptOO4Gm2n5D67SmqllsJrOtuYWu8ofB6707C9juVscku+uyk1Wa67afhlnruqu3GOu+t9/a677+FDlxwjglH2vClEXda8agZp9rxqyHXWvKuKa8c48sxn1fzkDkv2fOUQW9Z9JhJr9n0nFHvWfXVnW3ddS1gB1l2kmlH2XaWcYdZd5p5x9l3noEPftXhXTceX+T5VR5g5gl2HmHoGZYeYuoptv56QbPHfPt7u9/3+3/DH3j8g8tf+PyH0594ffZRyX2Cg1+15Ict+m3Lft7CX7j0Ry7+nct/6gJgAHc0wLIV8IBrS+AC49b/wAfeLYIT9EIFLTgiDBqtgGg7YALdtsAG0u2BEdTbBE14QgV9QofEk9YKN+hCD8YwhDS04Q1xmMMdYi4NS9yeE5zYPSpE8XtLoGL4hHDF8VFAi+UbQhfPBwYw+q4TYzyi8JLYsSWmkYlnc2Ibn6i2KMZRim2jYh2rCLcr5hGLc9NiH7doty4G0ot5A2Mhw5g6MpaRdWfUghoduUaQuVGSbySZHC05R5TZUZN3ZJkePblHmPlRlH+kmSBNOUicGVKVh6RgIhVpubE9UpaQnGQtKXlJXGJyk7vk5Cd9CcpRBpOUpyQmKld5TDCO0YivzBQmZvlMW0bTCbmkJhV4ec0l//xSm0IQZjcpUExwDgGZ4+QEM1fnzGfKUpq2rGYuscnLbf7Sm8IMZzHHicxymvNv6EynI9dZy3bi8p27jKcv5xnMehLznsfMpz7Dxs9+pvGfkwzoJQe6yYJ+8qCjTOgpF7rKhjo0aBCNaBomKsmKWvKimsyoJzcqyo6a8qOqDKlI30XSiJ7UjSmV40rt2FI9vtSPMRXkTA1ZU5v2Cqf91GkbeRpHn9YRqHkUah+JGkijFhKpSW1mSdXYVCc+NYpRpeJUr1hVLV61i1kF41a5uqalphOs0xQrWbNpVrR+U61sBYNb3zqluEITrGK1JlnNyk20qlWcbPXrX3WECch61f+kg62rYfGa2L1mlRONdexjI9tPPIRWp3EgLU/dcFqfvkG1QGVDa4UqB9gSVQ+z/ehmOdvZHUE2rqHlrWilSVrglraapyUuarGpWuSudputZa5rvQlb6MY2nLOlLm0Zulnc+k23auxtd30ryeCGV7iWLG55javJ5KZXuZ5sbnudK8roxle6pqxufa3bVuxmt3KR9W5/vyteAI/XvAM+r3oNvF73Jvi98mXwfO374NnaVr/XG4R/LRxgDMeBwBt2w4E9/AYFh5gNDSaxHCB84kBMmH0VtnB/MxxgDhP4wwcWsYJL3OATQzjFKg4ei1vc3RcDOMYDnrGBa5zgGzM4xw//3jGPK+fjH/M2yOIdsnmLrN4juzfJ8l2yfZvsZL9BOcp4mHJ4q1zeK6c3y+3dcny7XN8vg1loYo5ymYN75uKmOblrbm6bo/vm6sZZzhij84/tDFw8E1fPyOUzc/0MXUBTV9CDdlahW3xo0ib6tItWbaNb+2jYRnq2k6b0qix9YUxrusOc9vSIQS1qPZC61II6tX8xrWFNcxrEnga1iUUt61lTaRDDHvOwC3HsKR87Ecuu8rId8ewrPxsR087ytBVx7S1fOxDbfvO2gz1nYvt32IPgyrELgWFzc2XZidjwurnybEd4GN5cmTYiQlxvrlxbESTWN1e2HQgUe/vbYRt3cnfHDRZzhzfdX1l3ed39FXind95fqXd78f0Vfce331/5N5wFPvC/FfzgY0n4wsXS8IeLJeITF0vFLy6WjG9cLB3/N8hXPGzmmPwzKf8Myz/z8s/I/DM1t3nRjX50pCdd6UtnetOd/nSoR13qU6d61Z0eEAA7';

img.onload = function() {
    ctx.drawImage(img, 0, 0, img.width, img.height);
};


//todo optimize
$('#username').on('blur', function() {
    /* setName($(this).val());*/

    $('.card.menu > .header > img').addClass('excite');
    setTimeout(function() {
        $('.card.menu > .header > img').removeClass('excite');
    }, 800);

});

// Dirty Colorpicker
$('#colorpick').on('mousedown', function(eventDown) {
    var x = eventDown.offsetX;
    var y = eventDown.offsetY;

    if (eventDown.button === 0) {
        $('.card.menu > .header > img').addClass('excite');
        setTimeout(function() {
            $('.card.menu > .header > img').removeClass('excite');
        }, 800);

        var imgData = ctx.getImageData(x, y, 1, 1).data;
        localStorage.setItem('color', JSON.stringify(imgData));
        stylechange(imgData);
    }
});

$('.fa-paper-plane').on('click', function() {
	
    if ($('.chat-input').val() != '') {
        var data = {
            message: $('.chat-input').val(),
            message_to: $('#message-to-hidden').val(),
            message_from: window.localStorage.getItem("userid"),
            fromUserName: $("#friendName_header").text(),
            toUserFullName: $("#login-user-name").text(),
        };
        busy = true;
        $.ajax({
            url: getUrl() + "/restCalls/sendMessage.php",
            type: "post",
            crossDomain: true,
            data: data,
			global:false,
			beforeSend: function(){
				var html = '<li id="revers_rows"><div class="message" id="revers_div" style="word-wrap: break-word;">' + $('.chat-input').val() + '<div style=" font-size: 10px; text-align: right; color: rgba(0, 0, 0, 0.64); "><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="font-size:10px;color: #212f50;"></i></div></div></li>';
				$('ul.chat').append(html);
				$('.list-chat').scrollTop(1000);
			},
            success: function(data) {
				var res = $.parseJSON(data);
                if (res.ret == 1) {
					sendPushNotification();
                    var dp = getUrl() + "/avatar/" + window.localStorage.getItem("username") + "/small";
                    var html = '<li id="revers_rows"><div class="message" data-guid="'+res['guid']+'" id="revers_div" style="word-wrap: break-word;">' + res['message'] + '<div style=" font-size: 10px; text-align: right; color: rgba(0, 0, 0, 0.64); ">'+res['time']+'</div></div></li>';
                    $('ul.chat li:last').remove();
					$('ul.chat').append(html);
                    $('.list-chat').scrollTop(1000);
                }
            },
            error: function(xhr, status, error) {
                var dp = getUrl() + "/avatar/" + window.localStorage.getItem("username") + "/small";
                var html = '<li id="revers_rows"><div class="message" id="revers_div" style="word-wrap: break-word;">' + $('.chat-input').val() + '<div style=" font-size: 10px; text-align: right; color: rgba(0, 0, 0, 0.64); "><i class="fa fa-times" style="color:red;font-size:10px"></i></div></div></li>';
				$('ul.chat li:last').remove();
                $('ul.chat').append(html);
                $('.list-chat').scrollTop(1000);
            },
            complete: function() {
                busy = false;
            }
        });
        $('.chat-input').val('');
    }
});

$('.chat-input').on('keyup', function(event) {
    event.preventDefault();
    if (event.which === 13) {
        $('.fa-paper-plane').trigger('click');
    }
});

$('.list-text > ul > li').on('click', function() {
    var name = $(this).find('.name').text();
    $('ul.chat > li').eq(1).html('<img src="' + $(this).find('img').prop('src') + '"><div class="message"><p>' + $(this).find('.txt').text() + '</p></div>');

    // timeout just for eyecandy...
    setTimeout(function() {
        $("#username_header").text(name);
        $('.shown').removeClass('shown');
        $('.list-chat').addClass('shown');
        setRoute('.list-chat');
        $("html, body").animate({
            scrollTop: $(document).height()
        }, 0);
        $('.chat-input').focus();
    }, 0);
});

// List context
// Delegating for dom manipulated list elements
$('.list-account > .list').on('click', 'li', function() {
    $(this).parent().children().removeClass('active');
    $(this).parent().find('.context').remove();
    $(this).addClass('active');
    /*$('ul.chat > li').eq(1).html('<img src="' + $(this).find('img').prop('src') + '"><div class="message"><p>' + $(this).find('.txt').text() + '</p></div>');
    var name=$(this).find('.name').text();*/
    $('.shown').removeClass('shown');
	$("#username_header").text(name);
	$('.list-chat').addClass('shown');
	setRoute('.list-chat');
    //$('.chat-input').focus();
    $('#text_tab').addClass('active');
    $("#account_tab").removeClass('active');
    /*var $TARGET = $(this);
    if (!$(this).next().hasClass('context')) {
        var $ctx = $('<li class="context"><i class="mdi mdi-pencil"></i><i class="mdi mdi-delete"></i></li>');

        $ctx.on('click', '.mdi-pencil', function() {
            setModal('edit', $TARGET);

            $('#contact-modal').one('click', '.btn.save', function() {
                $TARGET.find('.name').text($('#new-user').val());
                closeModal();
            });
        });

        $ctx.on('click', '.mdi-delete', function() {
            $TARGET.remove();
        });


        $(this).after($ctx);
    }*/
});

// Navigation
$('.nav li').on('click', function() {
    $(this).parent().children().removeClass('active');
    $(this).addClass('active');
    $('.shown').removeClass('shown');
    var route = $(this).data('route');
    $(route).addClass('shown');
    setRoute(route);
	$('#no-recent-chat').remove();
});

$('#head').on('click', '.mdi-fullscreen', function() {
    $(this).removeClass('mdi-fullscreen').addClass('mdi-fullscreen-exit');
    $('#hangout').css({
        width: '900px'
    });
});

$('#head').on('click', '.mdi-fullscreen-exit', function() {
    $(this).removeClass('mdi-fullscreen-exit').addClass('mdi-fullscreen');
    $('#hangout').css({
        width: '400px'
    });
});

// menuclick
$('#head .fa-bars').on('click', function() {
    $('.menu').toggleClass('open');
    //$('.overlay').toggleClass('add');
});

// viewtoggle > 1000
$('#head .mdi-chevron-down').on('click', function() {
    if ($('#hangout').hasClass('collapsed')) {
        $(this).removeClass('mdi-chevron-up').addClass('mdi-chevron-down');
        $('#hangout').removeClass('collapsed');
    } else {
        $(this).removeClass('mdi-chevron-down').addClass('mdi-chevron-up');
        $('#hangout').addClass('collapsed');
    }

});

// Filter
$('.search-filter').on('keyup', function() {
    var filter = $(this).val();
    $(GLOBALSTATE.route + ' .list > li').filter(function() {
        var regex = new RegExp(filter, 'ig');

        if (regex.test($(this).text())) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
	searchAllUsers(filter);
});

//To search for all users of mychance
function searchAllUsers(key){
	$.ajax({
        url: getUrl() + "/restCalls/searchUsers.php",
        type: "POST",
        crossDomain: true,
		global:false,
        data: {
            'key': key,
			'guid' : window.localStorage.getItem('userid')
        },
		beforeSend: function(){
			
		},
        success: function(response) {
			$('.search-user-item').remove();
			if(response!="")
				$('#friend-list').append(response);
        },
        complete: function() {
			
        }
    });
}

// killit
$('#contact-modal').on('click', '.btn.cancel', function() {
    closeModal();
});

$('#new-user').on('keydown', function(event) {
    switch (event.which) {
        case 13:
            event.preventDefault();
            $('.btn.save').trigger('click');
            break;

        case 27:
            event.preventDefault();
            $('.btn.cancel').trigger('click');
            break;
    }

});

$('#add-contact-floater').on('click', function() {
    if ($(this).hasClass('active')) {
        closeModal();
        $(this).removeClass('active');

    } else {

        $(this).addClass('active');
        setModal('add');
        $('#contact-modal').one('click', '.btn.save', function() {
            $('.list-account > .list').prepend('<li><img src="http://lorempixel.com/100/100/people/1/"><span class="name">' + $('#new-user').val() + '</span><i class="mdi mdi-menu-down"></i></li>');
            closeModal();
        });
    }
});

$("#bottom-ul").on('click', function() {
    $('#message-list').html("");
})
$("#text_tab").on('click', function() {
	/*$("#friend_dp").attr("src","");
    $('#recent-chats').html("");
    loadRecentChats(window.localStorage.getItem("userid"));*/
});

$("#login").click(function(e) {
    var username = $("#userName").val();
    var password = $("#password").val();
    $.ajax({
        url: getUrl() + "/restCalls/login.php",
        type: "post",
        crossDomain: true,
		global:false,
        data: {
            userName: username,
            password: password,
            newToken: token
        },
		beforeSend: function(){
			 $('.emessage').html('').hide();
			$('#login').val("Validating..");
			$('#login').css('pointer-events', 'none');
		},
        success: function(response) {
            var obj = $.parseJSON(response);
            if (obj.isSuccess == true) {
                $("#loginPage").css("display", "none");
                window.localStorage.setItem('username', username);
                window.localStorage.setItem('password', password);
                window.localStorage.setItem("userid", obj.userid);
                loadHomePage(obj)
            } else {
                $('.emessage').html('Invaild credentials').fadeIn(200);
            }
        },
        complete: function() {
			$('#login').val("Login");
			$('#login').css('pointer-events', 'auto');
        }
    });

});

function showLogin() {
    closeSpinner();
    $("#loginPage").fadeIn(500);
}

function loadHomePage(data) {
	 var obj=data.friends;
	 var html = "";
	   for (var i = 0; obj[i]['id'] != "endofFriends"; i++) {
		var userid = obj[i]['id'];
		var fullName = obj[i]['name'];
		var isOnline = obj[i]['online'];
		var username = obj[i]['username'];
		var onlineClass = '';
		var dp = getUrl() + "/avatar/" + username + "/small/";
		if (isOnline == '1')
			onlineClass = 'online';
		html = html + "<li id='friend" + userid + "' data-username='" + username + "'><img src='" + dp + "'><span class='name'>" + fullName + "</span><i class='mdi mdi-menu-down'></i><div style='display:none'  class='" + onlineClass + "'></div></li>";
	}
	//if search is in progress, dont past html
	if($('.search-filter').val()==""){
		$("#friend-list").html(html);
	}
	window.localStorage.setItem('home_page_html', html);
	var myName = obj[i + 1]['name'];
	var myDp = getUrl() + "/avatar/" + window.localStorage.getItem('username') + "/large";
	setName(myName);
	setDp(myDp);
	$("#homePage").show();
}

function loadRecentChats(data) {
	console.log("herer");
	console.log(data.chats);
	var obj = data.chats;
	var html  = "";
	var yesterday = getYesterdaysDate();
	var today = getTodaysDate();
	if($.isEmptyObject(obj)){
		$('#recent-chats').after('<div id="no-recent-chat" style="height: 20px;font-size: 15px;text-align: center;font-weight: bold;position: fixed;top: 50%;left: 50%;margin-left: -53px;margin-top: -10px;width: 106px;">No recent chats</div>');
	}
	else{
		$('#no-recent-chat').remove();
	}
	for (var i = 0; obj[i] != null; i++) {
		var name = obj[i]['fullname'];
		var username = obj[i]['username'];
		var userid = obj[i]['guid'];
		var message = obj[i]['message']==null? "" : obj[i]['message'];
		console.log(message);
		var time = obj[i]['time'];
		
		if(obj[i]['date']==yesterday)
			time = "YESTERDAY";
		else if(obj[i]['date']!=yesterday && obj[i]['date']!=today)
			time = obj[i]['date'];		
		var dp = getUrl() + "/avatar/" + username + "/small";
		html = html + '<li id=chat' + userid + ' onclick = openchatBox(' + userid + ') data-userid=' + userid + ' data-username=' + username + '><img src="' + dp + '"><div class="content-container"><span class="name">' + name + '</span><span class="txt" style="font-size:12px">' + message + '</span></div><!--<p>&#x2606;</p>--><span class="time">' + time + '</span></li>';
	}
    $('#recent-chats').html(html);
	window.localStorage.setItem('recent_chat_html', html);
}

function refreshHome() {
    $.ajax({
        url: getUrl() + "/restCalls/homepage.php",
        type: "post",
        crossDomain: true,
		global:false,
        data: {
            id: window.localStorage.getItem('userid')
        },
        success: function(response) {
			var obj = $.parseJSON(response);
            var html = "";
            for (var i = 0; obj[i][0] != "endofFriends"; i++) {
                var userid = obj[i][0];
                var fullName = obj[i][1];
                var isOnline = obj[i][2];
                var username = obj[i][3];
                var onlineClass = '';
                var dp = getUrl() + "/avatar/" + username + "/small/";
                if (isOnline == '1')
                    onlineClass = 'online';
                html = html + "<li id='friend" + userid + "' data-username='" + username + "'><img src='" + dp + "'><span class='name'>" + fullName + "</span><i class='mdi mdi-menu-down'></i><div  class='" + onlineClass + "'></div></li>";
            }
			if($('.search-filter').val()=="")
				$("#friend-list").html(html);
            var myName = obj[i + 1][0];
            var myDp = getUrl() + "/avatar/" + window.localStorage.getItem('username') + "/large";
            setName(myName);
            setDp(myDp);
        },
        complete: function() {

        }
    });

}

function openchatBox(id) {
	window.localStorage.setItem('selected_friend', id);
	if($("li#friend" + id).length>0)
    	$("li#friend" + id).click();
	else{
		$('#bottom-ul').hide();
		document.addEventListener("backbutton", onBackKeyDown, false);
        var userid = id;
		$("#friend_dp").attr("src","");
        loadMessages(userid);
		$('.list-text').removeClass('shown');
		$('.list-chat').addClass('shown');
		$('#friend_dp').show();
		$('#head .fa.fa-bars').hide();
	}
    $('#select-media').show();
    setMessageToHidden(id);
}

function messageLoad(){
	if(!busy){
		var pos = $('.list-chat').scrollTop();
		if(pos<350){
			busy = true;
			offset = limit + offset;
			var friend_id = $('#message-to-hidden').val();
			loadMessages(friend_id);
		}
	}
}

function getTodaysDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();
	if(dd<10) {
		dd='0'+dd
	} 
	if(mm<10) {
		mm='0'+mm
	} 
	today = dd+'-'+mm+'-'+yyyy;
	return today;
}

function getYesterdaysDate(){
	var d = new Date();
	d.setDate(d.getDate() - 1);
	var dd = d.getDate();
	var mm = d.getMonth()+1;
	var yyyy = d.getFullYear();
	if(dd<10) {
		dd='0'+dd
	} 
	if(mm<10) {
		mm='0'+mm
	} 
	var yesterday = dd+'-'+mm+'-'+yyyy;
	return yesterday;
}

var loadAgain = true;
function loadMessages(friend_id) {
    if (friend_id != null && window.localStorage.getItem("userid") != null && loadAgain) {
        $.ajax({
            url: getUrl() + "/restCalls/loadMessages.php",
            type: "POST",
            crossDomain: true,
			global:false,
            data: {
                myId: window.localStorage.getItem("userid"),
                friendId: friend_id,
				limit:limit,
				offset:offset
            },
			beforeSend: function(){
				$('#loading_rows').remove();
				var ht = '<li id="loading_rows"><div class="time" style="width: 100px;position: relative;left: 50%;margin-left: -50px;padding: 5px;font-size: 10px;font-weight: bold;text-align: center;background: rgba(33, 47, 80, 0.23);color: white;"><div style="" class="message-div">Loading..</div></div></li>';
				if($('#message-list li:first').length)
					$('#message-list li:first').before(ht);
				else
					$('#message-list').html(ht);
			},
            success: function(response) {
				$('#loading_rows').remove();
				var obj = $.parseJSON(response);
                var myUserId = window.localStorage.getItem("userid");
                var message = "";
                var finalMessage = "";
                var dp = '';
                var rev = "";
                var reverse_id = "";
				var html ='';
				
                for (var i = 0; obj[i] != null; i++) {
                    message = obj[i]['message'];

                    if (obj[i]['message_from'] == myUserId) {
                       // dp = myDp;
                        rev = "revers_rows";
                        reverse_id = "revers_div";
                    } else {
                        //dp = friendDp;
                        rev = 'forward_rows';
                        reverse_id = 'normal_div';

                    }
					if((i!=0 && obj[i-1]['date']!=obj[i]['date'])){
						var date = obj[i]['date'];
						if(obj[i]['date']==getTodaysDate())
							date = 'TODAY';
						else if(obj[i]['date']==getYesterdaysDate())
							date = 'YESTERDAY';
						//console.log(date);
						if(date!='undefined')
							html = html + '<li id="time_rows"><div class="time" style=" width: 100px; position: relative; left: 50%; margin-left: -50px; padding: 5px; font-size: 15px; font-weight: bold; text-align: center; "><div style="border: solid 1px rgba(33, 47, 80, 0.23);background: rgba(235, 228, 213, 0.28);border-radius: 50px;padding: 1px;font-size:13px">'+date+'</div></div></li>';
					}
					
					html = html + '<li id="' + rev + '" data-date="'+obj[i]['date']+'"><div class="message" data-guid="'+obj[i]['guid']+'" id="' + reverse_id + '" style="word-wrap: break-word;">' + message + '<div style=" font-size: 10px; text-align: right; color: rgba(0, 0, 0, 0.64); ">'+obj[i]['time']+'</div></div></li>';
				}
				var firstMsg = $('.message:first').parent();
				var pos = firstMsg.scrollTop();
				$('#message-list').prepend(html);
				if(html!=''){
					if(offset==0)
						$(".list-chat").scrollTop($('#message-list li:last').offset().top);
					busy =false;
				}
				if(obj.length<limit){
					loadAgain = false;
					var date = $("#message-list li:first:not(#time_rows)").attr('data-date');
					if(date==getTodaysDate())
							date = 'TODAY';
					else if(date==getYesterdaysDate())
						date = 'YESTERDAY';
					console.log(date);
					if(typeof date!='undefined')
						var html = '<li id="time_rows" style=" background: rgba(33, 47, 80, 0.11); "><div class="time" style="width: 200px;position: relative;left: 50%;margin-left: -100px;padding: 5px;font-size: 15px;font-weight: bold;text-align: center;"><div style="background: rgba(235, 228, 213, 0.28);padding: 1px;font-size: 11px;">No more messages</div></div></li><li id="time_rows"><div class="time" style=" width: 100px; position: relative; left: 50%; margin-left: -50px; padding: 5px; font-size: 15px; font-weight: bold; text-align: center; "><div style="border: solid 1px rgba(33, 47, 80, 0.23);background: rgba(235, 228, 213, 0.28);border-radius: 50px;padding: 1px;font-size:13px">'+date+'</div></div></li>';
					$('#message-list').prepend(html);
					return false;
				}
				busy =false;
				if(offset!=0){
					//firstMsg.scrollTop(pos);
				}
            },
            complete: function() {
				busy =false;
			},
			error:function(){
				$('#loading_rows message-div').html('Network error').css('color', 'red');
			}
        });
    }

}

function showFilePicker() {
    window.plugins.mfilechooser.open([], function(uri) {
        

    }, function(error) {
       // alert(error);

    });
}
jQuery("input#file").change(function() {
    var data = new FormData();
    data.append('file', $('#file').prop('files')[0]);
    data.append('message_to', $("#message-to-hidden").val());
    data.append('message_from', window.localStorage.getItem("userid"));
    data.append('message_from_name', $("#friendName_header").text());
    data.append('message_to_name', $("#login-user-name").text());
    $.ajax({
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: data,
        url: getUrl() + "/restCalls/sendMedia.php",
        dataType: 'json',
		global:false,
		xhr: function(){
			myXhr = $.ajaxSettings.xhr();
			if (myXhr.upload) {
				myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
			}
			return myXhr;
		},
        beforeSend: function() {
			var html = '<li class="media-sending" id="revers_rows"><div class="message" id="revers_div" style="word-wrap: break-word;color:green;font-size:10px;width:60%">Sending..<div class="progress" style="margin-bottom:0px"> <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> 0% </div> </div></div></li>';
			$("#message-list").append(html);
			$('.list-chat').scrollTop(10000);
        },
        success: function(data) {
			try{
				var html = getMediaHtml(data);
				$("#message-list li.media-sending").after(html);
				$('.media-sending').remove();
				$('.list-chat').scrollTop(10000);
			}catch(err){
				$('.media-sending .message').html('Sending failed').css('color', 'red');
			}
        },
        error: function(xhr, status, error) {
            $('.media-sending .message').html('Sending failed').css('color', 'red');
        },
        complete: function() {

        }

    });
});

function getMediaHtml(data){
	//var data = $.parseJSON(res);
	var type = data.message_type;
	var source = data.source; 
	var guid = data.guid;
	var time = data.time;
	var message = "";
	if(type=="photo")
		message = '<img id=message-photo src="'+source+'" width="100px height=100px">';
	else if(type=="video")
		message = '<video class="message" preload=metadata controls> <source type=video/mp4 src="'+source+'"> <source type=video/webm src="'+source+'"> </video>';
	else if(type=='audio')
		message = '<audio controls class=message> <source src="'+source+'" type=audio/mp3> <source src="'+source+'" type=audio/ogg> <source src="'+source+'" type=audio/mpeg> <source src="'+source+'" type=audio/wav></audio>'
		
	var html = '<li id="revers_rows"><div class="message" data-guid="'+guid+'" id="revers_div" style="word-wrap: break-word;">' + message + '<div style=" font-size: 10px; text-align: right; color: rgba(0, 0, 0, 0.64); ">'+time+'</div></div></li>';
	return html;
}

function progressHandlingFunction(e) {
	if (e.lengthComputable) {
		var s = parseInt((e.loaded / e.total) * 100);
		var width = s+'%';
		$('.progress-bar.progress-bar-striped.active').attr('aria-valuenow', s)
													  .css('width', width)
													  .html(width);
	}
}



function checkNewMessage() {
    //console.log("checknew message");
    if ($('.list-chat').hasClass('shown')) {
        //console.log('inside_ajax');
        var message_to = window.localStorage.getItem("userid");
        var message_from = $("#message-to-hidden").val();
        $.ajax({
            type: 'POST',
            data: {
                message_from: message_from,
                message_to: message_to
            },
            url: getUrl() + "/restCalls/checkNewMessage.php",
            crossDomain: true,
			global:false,
            success: function(res) {
                var data = $.parseJSON(res);
                if (!(data == null || data == '')) {
                    var message = '';
					var html = "";
                    for (var i = 0; data[i] != null; i++) {
                        message = data[i]['message'];
						html = html + '<li id="forward_rows"><div class="message" id="normal_div" data-guid="'+data[i]['guid']+'" style="word-wrap: break-word;">' + message + '<div style=" font-size: 10px; text-align: right; color: rgba(0, 0, 0, 0.64); ">'+data[i]['time']+'</div></div></li>';
                    }
                    
                    $('#message-list').append(html);
                    $('.list-chat').scrollTop(10000);
                }

            },
            error: function(xhr, status, error) {
                //alert('error: '+xhr.responseText);
            },
            complete: function() {
                newMessageNotification();
            }

        });
    } else {
        newMessageNotification();
    }

}

function newMessageNotification() {
    //console.log('newMessageNotification');
    var id = window.localStorage.getItem("userid");
    if (id != '' && id != null && $("#homePage").css("display") == 'block') {
        $.ajax({
            type: 'POST',
            data: {
                id: id
            },
            url: getUrl() + "/restCalls/newMessageNotification.php",
            crossDomain: true,
			global:false,
            success: function(res) {
                //console.log(res);
                var obj = $.parseJSON(res);
                var msgcount = obj['count'];
                if (msgcount != '' || msgcount != '0')
                    $("#messagecount").html(msgcount).show();
                else
                    $("#messagecount").html("").hide();
                var arr = obj['senders'];
                for (var i = 0; arr[i] != null; i++) {
                    if ($("#chat" + arr[i]).attr('data-username') != '') {
                        //console.log(obj['message'][i]);
                        $('#chat' + arr[i]).find('.txt').html(obj['message'][i]);
                        //$('#chat'+arr[i]+'.txt').html(obj['message'][i]);
                        $("#chat" + arr[i]).prependTo("#recent-chats");
                        $("#chat" + arr[i]).css('background', 'beige');
                    }
                }
            },
            error: function(xhr, status, error) {

                //alert('error: '+xhr.responseText);
            },
            complete: function() {
                onlineCheck();
            }
        });
    }
}

function updateLastActivity() {
    //console.log("update last activity");	
    if ($("#homePage").css("display") == 'block') {
        $.ajax({
            type: 'POST',
            data: {
                id: window.localStorage.getItem("userid")
            },
            url: getUrl() + "/restCalls/updateActivity.php",
            crossDomain: true,
			global:false,
            success: function(res) {

            },
            error: function(xhr, status, error) {
                //alert('error: '+xhr.responseText);
            },
            complete: function() {
                checkNewMessage();
            }

        });
    }
}

function onlineCheck() {
	var friend_id;
	if($('div.list-chat').hasClass('shown')){
		friend_id = window.localStorage.getItem("selected_friend");
	}
	else
		return false;
	if (friend_id != '' && friend_id != null) {
		$.ajax({
			type: 'POST',
			data: {
				friend_id: friend_id
			},
			url: getUrl() + "/restCalls/isFriendOnline.php",
			crossDomain: true,
			global:false,
			success: function(res) {
				if($('.control-items').is(':visible'))
					return false;
			   if(res==true || res==1)
					$('.online-span').show();
				else
					$('.online-span').hide();
			},
			error: function(xhr, status, error) {
			
			},
			complete: function() {
			
			}

		});
	}
}

function appStartup() {
	if(window.localStorage.getItem('recent_chat_html')!=null){
		$('#recent-chats').html(window.localStorage.getItem('recent_chat_html'));
		$("#homePage").show();
	}
	if(window.localStorage.getItem('home_page_html')!=null){
		if($('.search-filter').val()=="")
			$("#friend-list").html(window.localStorage.getItem('home_page_html'));
		$("#homePage").show();
	}
    var username = window.localStorage.getItem("username");
    var password = window.localStorage.getItem("password");
    if (username != '' && password != '' && username != null && password != null) {
        $.ajax({
            url: getUrl() + "/restCalls/login.php",
            type: "post",
            crossDomain: true,
			global:false,
            data: {
                userName: username,
                password: password,
                newToken: window.localStorage.getItem("deviceId")
            },
            success: function(response) {
				console.log(response);
                var obj = $.parseJSON(response);
                if (obj.isSuccess == true) {
                    window.localStorage.setItem('username', username);
                    window.localStorage.setItem('password', password);
                    window.localStorage.setItem("userid", obj.userid);
					loadRecentChats(obj);
                    loadHomePage(obj);;
                } else {
                    $("#loginPage").fadeIn(500);
                    alert("Invalid username or password", donothing, 'Bad credentials', 'OK');
                }
            },
            complete: function() {

            }
        });
    } else {
        closeSpinner();
        $("#loginPage").fadeIn(500);
    }
}

function donothing() {}

$("#logout-btn").on('click', function() {
    var username = window.localStorage.getItem("username");
    $.ajax({
        url: getUrl() + "/restCalls/logout.php",
        type: "post",
        crossDomain: true,
        data: {
            userName: username,
            newToken: token
        },
        success: function(response) {
			window.localStorage.removeItem("username");
			window.localStorage.removeItem("password");
			window.localStorage.removeItem("deviceId");
        },
        error: function() {},
        complete: function() {

        }
    });
	window.localStorage.clear(); 
    $("#homePage").css('display', 'none');
    $("#friend-list").html("");
    $("#recent-chats").html("");
    $('.card').removeClass('open');
    $('#mychance-logo').attr('src', 'img/logo.png');
    $("#loginPage").css('display', 'block');
});

/**
* function to send push notification
*/
function sendPushNotification(){
	$.ajax({
        url:"http://innathecinema.com/pay/mychance/push/messenger.php",
        type: "POST",
        crossDomain: true,
		global:false,
        data: {
            "guid": $("#message-to-hidden").val()
        },
        success: function(response) {
			console.log(response);
        },
        error: function() {},
        complete: function() {

        }
    });
}

$(document).ready(function(e) {
    $(document).on('ajaxStart', function(){
		$('.loader-wrapper').show();
		$('#no-loader').css('pointer-events', 'none')
					   .css('opacity', '0.3');
	});
	$(document).on('ajaxComplete', function(){
		$('.loader-wrapper').hide();
		$('#no-loader').css('pointer-events', 'auto')
					   .css('opacity', '1');
	});
});

//Full screen view

$(document).on('click', '.message img', function(event){
	var html = '<i class="fa fa-arrow-left" style="position:fixed;color: white; padding: 15px;font-size: 20px;z-index: 1000;" onclick="hideFullscreen()"></i><img src="'+$(this).attr('src')+'" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width: 100%;">';
	$('#fullscreen').html(html).show();
});

/*$(document).on('click', '.message video', function(e){
	var html = '<i class="fa fa-arrow-left" style="position:fixed;color: white; padding: 15px;font-size: 20px;z-index: 1000;" onclick="hideFullscreen()"></i>';
	html =  html+ '<video style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width: 100%;" controls><source src="'+$(this).find('source').attr('src')+'" type="video/mp4"></vide0>';
	$('#fullscreen').html(html).show(300);
});*/

function hideFullscreen(){
	$('#fullscreen').html('').hide(100);
}

var selectedMessages = Array();
$(function(){
	
});

var selectedMessages = Array();
$(document).on('click', '.message', function(e) {
	if($(e.target).is('img') || $(e.target).is('video')){
		e.preventDefault();
		return;
	}
	var guid = $(this).attr('data-guid');
	if($.inArray(guid,selectedMessages) == -1){
		selectedMessages.push($(this).attr('data-guid'));
		$(this).parent().addClass('selected');
		$(this).css('opacity', '0.6');
	}
	else{
		selectedMessages = jQuery.grep(selectedMessages, function(value) {
		  	return value != guid;
		});
		$(this).parent().removeClass('selected');
		$(this).css('opacity', '1');
	}
	if(selectedMessages.length === 0){
		showDefault();
	}
	else{
		$('.default-items').hide();
		if($('#forward_rows.selected').length){
			$('.control-items').show();
			$('.fa-trash.control-items').hide();
		}
		else
			$('.control-items').show();
	}
}); 
function showDefault(){
	selectedMessages = [];
	$('.selected').removeClass('selected');
	$('.control-items').hide();
	$('.default-items:not(.fa.fa-bars.default-items)').show();
	$('.message').css('opacity', '1');
}

function deleteMessages(){
	if($.isEmptyObject(selectedMessages))
		return false;
	if(window.localStorage.getItem('username')==null || window.localStorage.getItem('password')==null){
		return false;
	}
	$.ajax({
        url: getUrl() + "/restCalls/delete_message.php",
        type: "post",
        crossDomain: true,
		global:false,
        data: {
            username: window.localStorage.getItem('username'),
            password: window.localStorage.getItem('password'),
            ids: JSON.stringify(selectedMessages)
        },
		beforeSend: function(){
			for(var i=0;selectedMessages[i]!=null;i++){
				$('.message[data-guid="'+selectedMessages[i]+'"] div').html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="font-size:10px;color: #212f50;"></i>');
			}
		},
        success: function(response) {
            if(response==true || response==1){
				for(var i=0;selectedMessages[i]!=null;i++){
					$('.message[data-guid="'+selectedMessages[i]+'"]').parent().remove();
				}
				selectedMessages = [];			
			}
			else{
				for(var i=0;selectedMessages[i]!=null;i++){
					$('.message[data-guid="'+selectedMessages[i]+'"] div').html('<span style="color:red">Delete failed</span>');
				}
			}
        },
        complete: function() {
			showDefault();
        },
		error:function(){
			for(var i=0;selectedMessages[i]!=null;i++){
				$('.message[data-guid="'+selectedMessages[i]+'"] div').html('<i class="fa fa-times"></i>');
			}
		}
    });
}
selectedFriends = Array();
function showFriendList(){
	var html = $('#friend-list').html();
	html = html.replace(/li id="/g, 'li id="fwd');
	$('#forward-list').html(html);
	$('#forward-list li').css('background', 'white');
	$('#forward-friends').show();
}

$(document).on('click', '#forward-list li', function(e){
	var guid = $(this).attr('id').slice(9);
	if($.inArray(guid,selectedFriends) != -1){
		//removed selection
		$('#friend'+guid).css('background', 'white');
		selectedFriends = jQuery.grep(selectedFriends, function(value) {
		  	return value != guid;
		});
		$(this).find('.forward-selected').remove();
		$('.seleted-friends-display span[data-guid="'+guid+'"]').remove();
		if($('.seleted-friends-display span').length==1)
			$('.seleted-friends-display span').html($('.seleted-friends-display span').html().slice(2));
	}
	else{
		//selected friend
		$('#friend'+guid).css('background', '#f4f4f4');
		selectedFriends.push(guid);
		$(this).find('img').after('<i class="fa fa-check forward-selected" style=" position: absolute; left: 50px; top: 40px; color: white; border-radius: 50px; background: #0f9d58; padding: 2px; font-size: 10px; "></i>');
		//add comma if span present
		if($('.seleted-friends-display').html()=="")
			$('.seleted-friends-display').html($('.seleted-friends-display').html()+"<span data-guid='"+guid+"'> "+$(this).find('.name').html()+"</span");
		else
			$('.seleted-friends-display').html($('.seleted-friends-display').html()+"<span data-guid='"+guid+"'>, "+$(this).find('.name').html()+"</span");
		
		//if single span, remove first comma
		$('.seleted-friends-display').animate({scrollLeft:'+=1500'},20);
	}
});

$(document).on('keyup', '.forward-search', function(e){
	if($(this).val()==""){
		$('#forward-list li').show();
		return false;
	}
	$('#forward-list li').hide();
	$('#forward-list li:Contains('+$(this).val()+')').show();
});

function closeFriendList(){
	$('#forward-friends').hide();
	showDefault();
	selectedFriends = [];
	$('.seleted-friends-display').html('');
	$('#forward-list').html('');
}
function shareMessages(){
	$('.seleted-friends-display').html('');
	if($.isEmptyObject(selectedMessages))
		return false;
	if(window.localStorage.getItem('username')==null || window.localStorage.getItem('password')==null){
		return false;
	}
	var copyArray = selectedMessages.slice();
	$.ajax({
        url: getUrl() + "/restCalls/share_message.php",
        type: "post",
        crossDomain: true,
		global:false,
        data: {
            username: window.localStorage.getItem('username'),
            password: window.localStorage.getItem('password'),
            messages: JSON.stringify(selectedMessages),
			friends: JSON.stringify(selectedFriends)
        },
		beforeSend: function(){
			$('#forward-friends').hide();
			for(var i=0;copyArray[i]!=null;i++){
				if($('.message[data-guid="'+copyArray[i]+'"] div .share-message').length==0)
					$('.message[data-guid="'+copyArray[i]+'"] div').append('<div class="share-message" style="color:rgb(33, 47, 80)">Forwarding..</div>');
				else{
					$('.message[data-guid="'+copyArray[i]+'"] div .share-message').html('Forwarding..').css('color', 'rgb(33, 47, 80)');
				}
			}
			showDefault();
		},
        success: function(response) {
            if(response==true || response==1){
				for(var i=0;copyArray[i]!=null;i++){
					if($('.message[data-guid="'+copyArray[i]+'"] div .share-message').length==0)
						$('.message[data-guid="'+copyArray[i]+'"] div').append('<div class="share-message" style="color:green">Forwarding success</div>');
					else{
						$('.message[data-guid="'+copyArray[i]+'"] div .share-message').html('Forwarding success').css('color', 'green');
					}
				}
				selectedMessages = [];			
			}
			else{
				for(var i=0;copyArray[i]!=null;i++){
					if($('.message[data-guid="'+copyArray[i]+'"] div .share-message').length==0)
						$('.message[data-guid="'+copyArray[i]+'"] div').append('<div class="share-message" style="color:red">Forwarding failed</div>');
					else{
						$('.message[data-guid="'+copyArray[i]+'"] div .share-message').html('Forwarding failed').css('color', 'red');
					}
				}
			}
        },
        complete: function() {
			showDefault();
        },
		error:function(){
			for(var i=0;copyArray[i]!=null;i++){
				if($('.message[data-guid="'+copyArray[i]+'"] div .share-message').length==0)
					$('.message[data-guid="'+copyArray[i]+'"] div').append('<div class="share-message" style="color:red">Forwarding failed</div>');
				else{
					$('.message[data-guid="'+copyArray[i]+'"] div .share-message').html('Forwarding failed').css('color', 'red');
				}
			}
		}
    });
}

$(document).on('click', '#head #friend_dp', function(){
	var html = '<i class="fa fa-arrow-left" style="position:fixed;color: white; padding: 15px;font-size: 20px;z-index: 1000;" onclick="hideFullscreen()"></i><img id="dp-fullscreen" src="'+$(this).attr('src').slice(0,-5)+'" onerror="dpError()" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width: 100%;">';
	$('#fullscreen').html(html).show();
});

// handle if dp loading failed
function dpError(){
	$('#dp-fullscreen').attr('src', $('#head #friend_dp').attr('src'));
}